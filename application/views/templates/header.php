<html>
<head>
	<title>MVC</title>
	<link rel="stylesheet" href="https://bootswatch.com/4/darkly/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css">
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container">
			
			<div id="navbar">
				<ul class="nav navbar-nav">
					<li><a href="<?php echo site_url('home'); ?>">Home</a></li>
					<li><a href="<?php echo site_url('about'); ?>">About</a></li>
					<li><a href="<?php echo site_url('/posts'); ?>">Blog</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="<?php echo base_url(); ?>posts/create">Create Post</a></li>
					<li><a href="<?php echo site_url('user_authentication/index'); ?>">Login</a></li>
					<li><a href="<?php echo site_url('user_authentication/signup'); ?>">Signup</a></li>
					<li><a href="<?php echo site_url('user_authentication/logout'); ?>">Logout</a></li>
                 <li><a href="<?php echo site_url('user_authentication/admin'); ?>">Admin</a></li>
				</ul>
			</div>
		</div>
	</nav>


                
                